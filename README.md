> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Patrick Duke

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationslocations)
    - provide git command descriptions 

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Tables and insert statements
    - Include indexes and foreign key SQL statements
    - Include *your* query result set, including grant statements
    - Tables populated with at least 5 records both locally and to the CCI server
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Log into Oracle
    - Create and populate Oracle tables
    - SQL solutions
    - Chapter questions

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Use MySQL Workbench
    - Create Tables and Insert Test Data
    - Provide ERD (screenshot and file)
    - SQL Questions
    - Chapter Questions

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Log into Microsoft SQL Server
    - Create Tables Insert Test Data
    - Provide ERD (screenshot and file)
    - SQL Questions
    - Chapter Questions
    
6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Log into Microsoft SQL Server
    - Update A4 DB with new tables and records
    - Provide ERD (screenshot and file)
    - SQL Questions
    - Chapter Questions

    
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Download and set up MongoDB server/client
    - Import .json test data file
    - Screenshots of MongoDB commands
    - Chapter questions