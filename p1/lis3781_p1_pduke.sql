-- -----------------------------------------------------
-- Schema pad19b
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `pad19b` ;

-- -----------------------------------------------------
-- Schema pad19b
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `pad19b` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `pad19b` ;

-- -----------------------------------------------------
-- Table `pad19b`.`person`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`person` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`person` (
  `per_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `per_ssn` BINARY(64) NULL,
  `per_salt` BINARY(64) NULL,
  `per_fname` VARCHAR(15) NOT NULL,
  `per_lname` VARCHAR(30) NOT NULL,
  `per_street` VARCHAR(30) NOT NULL,
  `per_city` VARCHAR(30) NOT NULL,
  `per_state` CHAR(2) NOT NULL,
  `per_zip` INT(9) ZEROFILL NOT NULL,
  `per_email` VARCHAR(100) NOT NULL,
  `per_dob` DATE NOT NULL,
  `per_type` ENUM('a', 'c', 'j') NOT NULL,
  `per_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`phone`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`phone` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`phone` (
  `phn_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT NOT NULL,
  `phn_num` BIGINT NOT NULL,
  `phn_type` ENUM('c', 'h', 'w') NOT NULL,
  `phn_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`phn_id`),
  INDEX `fk_phone_person_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_phone_person`
    FOREIGN KEY (`per_id`)
    REFERENCES `pad19b`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`attorney`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`attorney` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`attorney` (
  `per_id` SMALLINT NOT NULL,
  `aty_start_date` DATE NOT NULL,
  `aty_end_date` DATE NULL,
  `aty_hourly_rate` DECIMAL(5,2) NOT NULL,
  `aty_years_in_practice` TINYINT NOT NULL,
  `aty_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_attorney_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `pad19b`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`bar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`bar` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`bar` (
  `bar_id` TINYINT NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT NOT NULL,
  `bar_name` VARCHAR(45) NOT NULL,
  `bar_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`bar_id`),
  INDEX `fk_bar_attorney1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_bar_attorney1`
    FOREIGN KEY (`per_id`)
    REFERENCES `pad19b`.`attorney` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`speciality`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`speciality` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`speciality` (
  `spc_id` TINYINT NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT NOT NULL,
  `spc_type` VARCHAR(45) NOT NULL,
  `spc_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`spc_id`),
  INDEX `fk_speciality_attorney1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_speciality_attorney1`
    FOREIGN KEY (`per_id`)
    REFERENCES `pad19b`.`attorney` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`court`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`court` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`court` (
  `crt_id` TINYINT NOT NULL AUTO_INCREMENT,
  `crt_name` VARCHAR(45) NOT NULL,
  `crt_street` VARCHAR(30) NOT NULL,
  `crt_city` VARCHAR(30) NOT NULL,
  `crt_state` CHAR(2) NOT NULL,
  `crt_zip` INT(9) ZEROFILL NOT NULL,
  `crt_phone` BIGINT NOT NULL,
  `crt_email` VARCHAR(100) NOT NULL,
  `crt_url` VARCHAR(100) NOT NULL,
  `crt_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`crt_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`judge`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`judge` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`judge` (
  `per_id` SMALLINT NOT NULL,
  `crt_id` TINYINT NULL,
  `jud_salary` DECIMAL(8,2) NOT NULL,
  `jud_years_in_practice` TINYINT NOT NULL,
  `jud_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  INDEX `fk_judge_court1_idx` (`crt_id` ASC) VISIBLE,
  CONSTRAINT `fk_judge_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `pad19b`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_judge_court1`
    FOREIGN KEY (`crt_id`)
    REFERENCES `pad19b`.`court` (`crt_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`judge_hist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`judge_hist` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`judge_hist` (
  `jhs_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT NOT NULL,
  `jhs_crt_id` TINYINT NULL,
  `jhs_date` TIMESTAMP NOT NULL,
  `jhs_type` ENUM('i', 'u', 'd') NOT NULL,
  `jhs_salary` DECIMAL(8,2) NOT NULL,
  `jhs_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`jhs_id`),
  INDEX `fk_judge_hist_judge1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_judge_hist_judge1`
    FOREIGN KEY (`per_id`)
    REFERENCES `pad19b`.`judge` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`case`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`case` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`case` (
  `cse_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `per_id` SMALLINT NOT NULL,
  `cse_type` VARCHAR(45) NOT NULL,
  `cse_description` TEXT NOT NULL,
  `cse_start_date` DATE NOT NULL,
  `cse_end_date` DATE NULL,
  `cse_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cse_id`),
  INDEX `fk_case_judge1_idx` (`per_id` ASC) VISIBLE,
  CONSTRAINT `fk_case_judge1`
    FOREIGN KEY (`per_id`)
    REFERENCES `pad19b`.`judge` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`client` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`client` (
  `per_id` SMALLINT NOT NULL,
  `cli_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`per_id`),
  CONSTRAINT `fk_client_person1`
    FOREIGN KEY (`per_id`)
    REFERENCES `pad19b`.`person` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `pad19b`.`assignment`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `pad19b`.`assignment` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `pad19b`.`assignment` (
  `asn_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `per_cid` SMALLINT NOT NULL,
  `per_aid` SMALLINT NOT NULL,
  `cse_id` SMALLINT NOT NULL,
  `asn_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`asn_id`),
  INDEX `fk_assignment_client1_idx` (`per_cid` ASC) VISIBLE,
  INDEX `fk_assignment_attorney1_idx` (`per_aid` ASC) VISIBLE,
  INDEX `fk_assignment_case1_idx` (`cse_id` ASC) VISIBLE,
  CONSTRAINT `fk_assignment_client1`
    FOREIGN KEY (`per_cid`)
    REFERENCES `pad19b`.`client` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignment_attorney1`
    FOREIGN KEY (`per_aid`)
    REFERENCES `pad19b`.`attorney` (`per_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_assignment_case1`
    FOREIGN KEY (`cse_id`)
    REFERENCES `pad19b`.`case` (`cse_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Data for 'person' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers', '437 Southern Dr', 'Rochester', 'NY', 324402222, 'srogers@comcast.net', '1923-10-03', 'c', NULL),
(NULL, NULL, NULL, 'Bruce', 'Wayne', '1007 Mountain Dr', 'Gotham', 'NY', 003208440, 'bwayne@gmail.com', '1968-03-20', 'c', NULL),
(NULL, NULL, NULL, 'Peter', 'Parker', '20 Ingram St', 'New York', 'NY', 102862341, 'pparker@msn.com', '1988-09-12', 'c', NULL),
(NULL, NULL, NULL, 'Jane', 'Thompson', '135 Ocean View Dr', 'Seattle', 'WA', 032084409, 'jthompson@gmail.com', '1978-05-08', 'c', NULL),
(NULL, NULL, NULL, 'Debra', 'Steele', '543 Oak Ln', 'Milwaukee', 'WI', 286234178, 'dsteele@verizon.net', '1994-07-19', 'c', NULL),
(NULL, NULL, NULL, 'Tony', 'Stark', '332 Palm Ave', 'Malibu', 'CA', 902638332, 'tstart@yahoo.com', '1972-05-04', 'a', NULL),
(NULL, NULL, NULL, 'Hank', 'Pymil', '2355 Brown St', 'Cleveland', 'OH', 022348890, 'hpym@aol.com', '1980-08-28', 'a', NULL),
(NULL, NULL, NULL, 'Bob', 'Best', '492 Avendale Ave', 'Scottsdale', 'AZ', 872638332, 'bbest@yahoo.com', '1992-02-10', 'a', NULL),
(NULL, NULL, NULL, 'Sandra', 'Dole', '8791 Lawrence Ave', 'Atlanta', 'GA', 002348890, 'sdole@gmail.com', '1990-01-26', 'a', NULL),
(NULL, NULL, NULL, 'Ben', 'Avery', '6432 Thunder Ln', 'Sioux Falls', 'SD', 562638332, 'bavery@hotmail.com', '1983-12-24', 'a', NULL),
(NULL, NULL, NULL, 'Arthur', 'Curry', '3304 Euclid Ave', 'Miami', 'FL', 000219932, 'acurry@gmail.com', '1975-12-15', 'j', NULL),
(NULL, NULL, NULL, 'Diana', 'Price', '944 Green St', 'Las Vegas', 'NV', 332048823, 'dprice@symaptico.com', '1980-08-22', 'j', NULL),
(NULL, NULL, NULL, 'Adam', 'Jurris', '98435 Valencia Dr', 'Gulf Shores', 'AL', 870219932, 'ajurris@gmx.com', '1995-01-31', 'j', NULL),
(NULL, NULL, NULL, 'Judy', 'Sleen', '56343 Rover Ct', 'Billings', 'MT', 672048823, 'jsleen@symaptico.com', '1970-03-22', 'j', NULL),
(NULL, NULL, NULL, 'Bill', 'Neiderheim', '4375 Netherland Rd', 'South Bend', 'IN', 320219932, 'bneiderheim@comcast.net', '1982-03-13', 'j', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for 'phone' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
(NULL, 1, 8503228827,'c', NULL),
(NULL, 2, 2052528929,'h', NULL),
(NULL, 3, 1034685598,'w', NULL),
(NULL, 4, 6402235345,'w', NULL),
(NULL, 5, 6402335886,'c', NULL),
(NULL, 6, 5506329842,'h', NULL),
(NULL, 7, 8202052203,'w', NULL),
(NULL, 8, 4008338594,'h', NULL),
(NULL, 9, 7655324489,'h', NULL),
(NULL, 10, 5463721984,'c', NULL),
(NULL, 11, 4537821902,'c', NULL),
(NULL, 12, 7868849203,'h', NULL),
(NULL, 13, 4589762251,'w', NULL),
(NULL, 14, 3721821902,'w', NULL),
(NULL, 15, 9177821945,'c', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for 'client' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO client
(per_id, cli_notes)
VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for 'attorney' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES
(6, '2006-06-12', NULL, 85, 5, NULL),
(7, '2003-08-20', NULL, 130, 28, NULL),
(8, '2009-12-12', NULL, 70, 17, NULL),
(9, '2008-06-08', NULL, 78, 13, NULL),
(10, '2011-09-12', NULL, 60, 24, NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for 'bar' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO bar
(bar_id, per_id, bar_name, bar_notes)
VALUES
(NULL, 6, 'Florida Bar', NULL),
(NULL, 7, 'Alabama Bar', NULL),
(NULL, 8, 'Georgia Bar', NULL),
(NULL, 9, 'Michigan Bar', NULL),
(NULL, 10, 'South Carolina Bar', NULL),
(NULL, 6, 'Montana Bar', NULL),
(NULL, 7, 'Arizona Bar', NULL),
(NULL, 8, 'Nevada Bar', NULL),
(NULL, 9, 'New York Bar', NULL),
(NULL, 10, 'New Jersey Bar', NULL),
(NULL, 6, 'Mississippi Bar', NULL),
(NULL, 7, 'California Bar', NULL),
(NULL, 8, 'Illinois Bar', NULL),
(NULL, 9, 'Indiana Bar', NULL),
(NULL, 10, 'Wisconsin Bar', NULL),
(NULL, 6, 'Minnesota Bar', NULL),
(NULL, 7, 'Maine Bar', NULL),
(NULL, 8, 'Oregon Bar', NULL),
(NULL, 9, 'Tennessee Bar', NULL),
(NULL, 10, 'Virgina Bar', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for 'speciality' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO speciality
(spc_id, per_id, spc_type, spc_notes)
VALUES
(NULL, 6, 'business', NULL),
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankruptcy', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'judicial', NULL),
(NULL, 6, 'environmental', NULL),
(NULL, 7, 'criminal', NULL),
(NULL, 8, 'real estate', NULL),
(NULL, 9, 'malpractice', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for 'court' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES
(NULL, 'leon county circuit court', '301 south monroe street', 'tallahassee', 'fl', 323035292, 8506065504, 'lccc@us.fl.gov', 'http://www.leoncountycircuitcourt.gov/', NULL),
(NULL, 'leon county traffic court', '500 south duval street', 'tallahassee', 'fl', 323035292, 8505774100, 'lctc@us.fl.gov', 'http://www.leoncountytrafficcourt.gov/', NULL),
(NULL, 'florida supreme court', '1921 thomasville road', 'tallahassee', 'fl', 323035292, 8504880125, 'fsc@us.fl.gov', 'http://www.floridasupremecourt.org/', NULL),
(NULL, 'orange county courthouse', '424 north orange avenue', 'orlando', 'fl', 328012248, 4078362000, 'occ@us.fl.gov', 'http://www.ninthcircuit.org/', NULL),
(NULL, 'fifth district court of appeal', '300 south beach street', 'daytona beach', 'fl', 321158763, 3862258600, '5dca@us.fl.gov', 'http://www.5dca.org/', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for 'judge' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
( 11, 5, 150000, 10, NULL),
( 12, 4, 185000, 3, NULL),
( 13, 4, 135000, 2, NULL),
( 14, 3, 170000, 6, NULL),
( 15, 1, 120000, 1, NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for 'judge_hist' table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(NULL, 11, 3, '2009-01-16', 'i', 130000, NULL),
(NULL, 12, 2, '2010-05-27', 'i', 140000, NULL),
(NULL, 13, 5, '2000-01-02', 'i', 115000, NULL),
(NULL, 13, 4, '2005-07-05', 'i', 135000, NULL),
(NULL, 14, 4, '2008-12-09', 'i', 155000, NULL),
(NULL, 15, 1, '2011-03-17', 'i', 120000, 'freshman justice'),
(NULL, 11, 5, '2010-07-05', 'i', 150000, 'assigned to another court'),
(NULL, 12, 4, '2012-10-08', 'i', 165000, 'became chief justice'),
(NULL, 14, 3, '2009-04-19', 'i', 170000, 'reassigned to court based upon local area growth');

COMMIT;

-- -----------------------------------------------------
-- Data for `case` table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil', 'client says his logo is being used without consent', '2010-09-09', NULL, 'copyright infringement'), 
(NULL, 12, 'criminal', 'client is charged with assault and battery', '2009-11-18', '2010-12-23', 'assault'), 
(NULL, 14, 'civil', 'client broke an ankle at local grocery store', '2008-05-06', '2008-07-23', 'injury'), 
(NULL, 11, 'criminal', 'client was stealing from place of employment', '2011-05-20', NULL, 'grand theft'), 
(NULL, 15, 'criminal', 'client was charged with posession of 10 grams of cocaine', '2011-06-05', NULL, 'posession of narcotics');

COMMIT;

-- -----------------------------------------------------
-- Data for assignment table
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 3, NULL),
(NULL, 1, 6, 3, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 1, NULL);

COMMIT;

-- -----------------------------------------------------
-- Procedure for generating and inserting random salted SSN numbers
-- -----------------------------------------------------

DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN

DECLARE x, y INT;
SET x = 1;
select count(*) into y from person;

WHILE x <= y DO 

SET @salt=RANDOM_BYTES(64);
SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512));

update person
set per_ssn=@ssn, per_salt=@salt
where per_id=x;

SET x = x + 1;

END WHILE;

END $$
DELIMITER ;
call CreatePersonSSN();