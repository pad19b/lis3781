> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Patrick Duke

### Assignment P1 Requirements:

*Five Parts:*

1. Use MySQL Workbench
2. Create Tables and Insert Test Data
3. Provide ERD (screenshot and file)
4. SQL Questions
5. Chapter Questions

#### README.md file should include the following items:

* Screenshot of ERD
* Link to p1.mwb
* Link to p1.sql

#### Assignment Screenshots:

*Screenshot ERD*:

![ERD Screenshot](img/ERD.png)

#### Links:

*Link to p1.mwb*
[p1.mwb File](lis3781_p1_pduke.mwb "P1 ERD in .mwb format")

*Link to p1.sql*
[p1.sql File](lis3781_p1_pduke.sql "P1 SQL Script")
