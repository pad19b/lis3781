> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Patrick Duke

### Assignment P2 Requirements:

*Four Parts:*

1. Download and set up MongoDB server/client
2. Import .json test data file
3. Screenshots of MongoDB commands
4. Chapter questions

#### README.md file should include the following items:

* Screenshot of MongoDB commands

#### Assignment Screenshots:

*Screenshot of Command #0*: Show database and collections you will be working on.

![Command #0 Screenshot](img/JSON0.png)

*Screenshot of Command #1*: Display all documents in collection.

![Command #1 Screenshot](img/JSON1.png)

*Screenshot of Command #2*: Display the number of documents in collection.

![Command #2 Screenshot](img/JSON2.png)

*Screenshot of Command #3*: Retrieve 1st 5 documents.

![Command #3 Screenshot](img/JSON3.png)

*Screenshot of Command #4*: Retrieve restaurants in the Brooklyn borough.

![Command #4 Screenshot](img/JSON4.0.png)

![Command #4 Screenshot](img/JSON4.1.png)

*Screenshot of Command #5*: Retrieve restaurants whose cuisine is American.

![Command #5 Screenshot](img/JSON5.0.png)

![Command #5 Screenshot](img/JSON5.1.png)


*Screenshot of Command #6*: Retrieve restaurants whose borough is Manhattan and cuisine is hamburgers.

![Command #6 Screenshot](img/JSON6.png)

*Screenshot of Command #7*: Display the number of restaurants whose borough is Manhattan and cuisine is hamburgers.

![Command #7 Screenshot](img/JSON7.PNG)

*Screenshot of Command #8*: Retrieve restaurants in the 10075 zip code area.

![Command #8 Screenshot](img/JSON8.0.png)

![Command #8 Screenshot](img/JSON8.1.png)

*Screenshot of Command #9*: Retrieve restaurants whose cuisine is chicken and zip code is 10024.

![Command #9 Screenshot](img/JSON9.png)

*Screenshot of Command #10*: Retrieve restaurants whose cuisine is chicken or whose zip code is 10024.

![Command #10 Screenshot](img/JSON10.png)

*Screenshot of Command #11*: Retrieve restaurants whose borough is Queens, cuisine is Jewish/kosher, sort by descending order of zipcode.

![Command #11 Screenshot](img/JSON11.png)

*Screenshot of Command #12*: Retrieve restaurants with a grade A.

![Command #12 Screenshot](img/JSON12.png)

*Screenshot of Command #13*: Retrieve restaurants with a grade A, displaying only collection id, restaurant name, and grade.

![Command #13 Screenshot](img/JSON13.png)

*Screenshot of Command #14*: Retrieve restaurants with a grade A, displaying only restaurant name, and grade (no collection id):

![Command #14 Screenshot](img/JSON14.png)

*Screenshot of Command #15*: Retrieve restaurants with a grade A, sort by cuisine ascending, and zip code descending.

![Command #15 Screenshot](img/JSON15.png)

*Screenshot of Command #16*: Retrieve restaurants with a score higher than 80.

![Command #16 Screenshot](img/JSON16.png)

*Screenshot of Command #17*: Insert a record with the following data:
    - street = 7th Avenue
    - zip code = 10024
    - building = 1000
    - coord = -58.9557413, 31.7720266
    - borough = Brooklyn
    - cuisine = BBQ
    - date = 2015-11-05T00:00:00Z
    - grade" : C
    - score = 15
    - name = Big Tex
    - restaurant_id = 61704627

![Command #17 Screenshot](img/JSON17.0.png)

![Command #17 Screenshot](img/JSON17.1.png)

*Screenshot of Command #18*: Update the following record:
Change the first White Castle restaurant document's cuisine to "Steak and Sea Food," and update
the lastModified field with the current date.

![Command #18 Screenshot](img/JSON18.0.png)

![Command #18 Screenshot](img/JSON18.1.png)

![Command #18 Screenshot](img/JSON18.2.png)

*Screenshot of Command #19*: Delete the following records:
    - Delete all White Castle restaurants.

![Command #19 Screenshot](img/JSON19.0.png)

![Command #19 Screenshot](img/JSON19.1.png)

