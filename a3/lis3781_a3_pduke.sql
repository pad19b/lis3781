SET DEFINE OFF

--auto increment cus_id
DROP SEQUENCE seq_cus_id;
Create Sequence seq_cus_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

--create customer table
drop table customer CASCADE CONSTRAINTS PURGE;
CREATE TABLE customer
(
    cus_id number(3,0) not null,
    cus_fname varchar2(15) not null,
    cus_lname varchar2(30) not null,
    cus_street varchar2(30) not null,
    cus_city varchar2(30) not null,
    cus_state char(2) not null,
    cus_zip number(5) not null,
    cus_phone number(10) not null,
    cus_email varchar2(100),
    cus_balance number(7,2),
    cus_notes varchar2(255),
    CONSTRAINT pk_customer PRIMARY KEY(cus_id)
);

--auto increment com_id
DROP SEQUENCE seq_com_id;
Create Sequence seq_com_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

--create commodity table
drop table commodity CASCADE CONSTRAINTS PURGE;
CREATE TABLE commodity
(
    com_id number not null,
    com_name varchar2(20),
    com_price number(8,2) not null,
    cus_notes varchar2(255),
    CONSTRAINT pk_commodity PRIMARY KEY(com_id),
    CONSTRAINT uq_com_name UNIQUE(com_name)
);

--auto inrement ord_id
DROP SEQUENCE seq_ord_id;
Create Sequence seq_ord_id
start with 1
increment by 1
minvalue 1
maxvalue 10000;

--create order table
drop table "order" CASCADE CONSTRAINTS PURGE;
CREATE TABLE "order"
(
    ord_id number(4,0) not null,
    cus_id number,
    com_id number,
    ord_num_units number(5,0) not null,
    ord_total_cost number(8,2) not null,
    ord_notes varchar2(255),
    CONSTRAINT pk_order PRIMARY KEY(ord_id),
    CONSTRAINT fk_order_customer FOREIGN KEY (cus_id) REFERENCES customer(cus_id),
    CONSTRAINT fk_order_commodity FOREIGN KEY (com_id) REFERENCES commodity(com_id),
    CONSTRAINT check_unit CHECK(ord_num_units > 0),
    CONSTRAINT check_total CHECK(ord_total_cost > 0)
);

--Insert data into customer table
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Bevis', 'Davis', '123 Main St', 'Detroit', 'MI', 48252, 3135551212, 'bdavis@alo.com', 11500.99, 'recently moved');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Steve', 'Taylor', '456 Eagle St', 'St. Louis', 'MO', 57252, 4185551212, 'staylor@gmail.com', 25.01, null);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Donna', 'Carter', '789 Pearl Ave', 'Los Angeles', 'CA', 48252, 3135551212, 'dcarter@gmail.com', 300.99, 'returning customer');
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Bob', 'Silverman', '857 Welby Rd', 'Phoenix', 'AZ', 25278, 4805551212, 'rsilverman@gmail.com', null, null);
INSERT INTO customer VALUES (seq_cus_id.nextval, 'Sarah', 'Victors', '534 Holly Way', 'Charleston', 'WV', 78345, 9045551212, 'svictors@aol.com', 500.76, 'new customer');
commit;

--Insert data into customer table
INSERT INTO commodity VALUES (seq_com_id.nextval, 'DVD Player', 109.00, null);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Cereal', 3.00, 'sugar free');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Scrabble', 29.00, 'original');
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Licorice', 1.89, null);
INSERT INTO commodity VALUES (seq_com_id.nextval, 'Tums', 6.45, 'antacid');
commit;

--Insert data into "order" table
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 50, 200, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 3, 30, 100, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 1, 6, 654, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 4, 24, 972, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 7, 300, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 1, 2, 5, 15, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 2, 3, 40, 57, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 1, 4, 300, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 5, 4, 14, 770, null);
INSERT INTO "order" VALUES (seq_ord_id.nextval, 3, 5, 15, 883, null);
commit;

select * from customer;
select * from commodity;
select * from "order";