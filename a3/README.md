> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Patrick Duke

### Assignment A3 Requirements:

*Four Parts:*

1. Log into Oracle
2. Create and populate Oracle tables
3. SQL solutions
4. Chapter questions

#### README.md file should include the following items:

* Screenshot of SQL code
* Screenshot of populated tables

#### Assignment Screenshots:

*Screenshot of SQL code*:

![SQL code Screenshot](img/code_a.png)

![SQL code Screenshot](img/code_b.png)

![SQL code Screenshot](img/code_c.png)

*Screenshot of populated tables*:

![Populated Tables Screenshot](img/pop_tables.png)

