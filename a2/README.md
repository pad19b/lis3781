> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Patrick Duke

### Assignment A2 Requirements:

*Four Parts:*

1. Tables and insert statements
2. Include indexes and foreign key SQL statements
3. Include *your* query result set, including grant statements
4. Tables populated with at least 5 records both locally and to the CCI server

#### README.md file should include the following items:

* Screenshot of *your* SQL code
* Screenshot of *your* populated tables

#### Assignment Screenshots:

*Screenshot of lis3781_a2_solutions on localhost*:

![SQL Code Screenshoot 1](img/lis3781_a2_code1.png)

![SQL Code Screenshoot 2](img/lis3781_a2_code2.png)

*Screenshot of lis3781 A2 Populated Tables*:

![Populated Tables Screenshot](img/lis3781_a2_tables.png)
