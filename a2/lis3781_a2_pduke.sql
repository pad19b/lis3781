drop database if exists pad19b;
create database if not exists pad19b;
use pad19b;

-- ---------------------------------
-- Table cpmpany
-- ---------------------------------
drop Table if exists company;
create Table if not exists company
(
    cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'),
    cmp_street VARCHAR(30) NOT NULL,
    cmp_city VARCHAR(30) NOT NULL,
    cmp_state CHAR(2) NOT NULL,
    cmp_zip int(9) UNSIGNED ZEROFILL NOT NULL COMMENT 'no dashes',
    cmp_phone BIGINT UNSIGNED NOT NULL COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
    cmp_ytd_sales DECIMAL(10,2) unsigned NOT NULL COMMENT '12,345,678.90',
    cmp_email VARCHAR(100) NULL,
    cmp_url VARCHAR(100) NULL,
    cmp_notes VARCHAR(255) NULL,
    PRIMARY KEY (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

Insert Into company
Values
(null,'C-Corp','507 - 20th Ave E Apt 2A','Seattle','WA','081226749','2065559857','12345678.00',null,'https://ezellschicken.com/','Ezells Famous Chicken'),
(null,'S-Corp','1250 Pacific Ave #101','Tacoma','WA','984021101','2533832214','9945678.00',null,'https://www.osf.com/location/tacoma-wa/','The Old Spaghetti Factory'),
(null,'Non-Profit-Corp','1414 Market St #100','Kirkland','WA','980331002','4258275111','1345678.00',null,'https://www.smarttalent.net/','Smart Talent'),
(null,'LLC','7530 164th Ave NE STE A108','Redmond','WA','980521102','4253762998','678345.58',null,'https://haikubuffet.com/','Haiku Sushi'),
(null,'Partnership','3000 1st Ave','Seattle','WA','981210221','2062035199','345679.86',null,'https://www.walthew.com/','The Walthew Law Firm');

SHOW WARNINGS;

-- ---------------------------------
-- Table customer
-- ---------------------------------
drop Table if exists customer;
create Table if not exists customer
(
    cus_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    cmp_id INT UNSIGNED NOT NULL,
    cus_ssn binary(64) NOT NULL,
    cus_salt binary (64) NOT NULL COMMENT '*only* demo purposes - do NOT use salt in the name!',
    cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
    cus_first VARCHAR(15) NOT NULL,
    cus_last VARCHAR(30) NOT NULL,
    cus_street VARCHAR(30) NULL,
    cus_city VARCHAR(30) NULL,
    cus_state CHAR(2) NULL,
    cus_zip INT(9) UNSIGNED ZEROFILL NULL,
    cus_phone BIGINT UNSIGNED NOT NULL COMMENT 'ssn and zip can be zero-filled but not US area codes',
    cus_email VARCHAR(100) NULL,
    cus_balance DECIMAL(7,2) UNSIGNED NULL COMMENT '12,345.67',
    cus_tot_sales DECIMAL(7,2) UNSIGNED NULL,
    cus_notes VARCHAR(255) NULL,
    PRIMARY KEY (cus_id),

    UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
    INDEX idx_cmp_id (cmp_id ASC),

    CONSTRAINT fk_customer_company
    FOREIGN KEY (cmp_id)
    REFERENCES company (cmp_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

SET @salt=RANDOM_BYTES(64);

Insert INTO customer
Values
(null,2,unhex(SHA2(CONCAT(@salt, 000456789),512)),@salt,'Discount','Wilbur','Denaway','23 Billings Gate','El Paso','TX','085703412','2145559857','test1@mymail.com','8391.87','37642.00','customer notes1'),
(null,4,unhex(SHA2(CONCAT(@salt, 001456789),512)),@salt,'Loyal','Bradford','Casis','891 Drift Dr','Stanton','TX','005819045','2145559482','test2@mymail.com','675.57','87341.00','customer notes2'),
(null,3,unhex(SHA2(CONCAT(@salt, 002456789),512)),@salt,'Impulse','Valerie','Lieblong','421 Calamari Vista','Odessa','TX','000621134','2145553412','test3@mymail.com','8730.23','78345.00','customer notes3'),
(null,5,unhex(SHA2(CONCAT(@salt, 003456789),512)),@salt,'Need-Based','Kathy','Jeffries','915 Drive Past Ln','Penwell','TX','009135674','2145558122','test4@mymail.com','2651.19','92678.00','customer notes4'),
(null,1,unhex(SHA2(CONCAT(@salt, 004456789),512)),@salt,'Wandering','Steve','Rogers','329 Volume Ave','Tarzan','TX','000054426','2145551189','test5@mymail.com','782.73','23471.00','customer notes5');

SHOW WARNINGS;

select * from company;
select * from customer;