> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Patrick Duke

### Assignment A5 Requirements:

*Five Parts:*

1. Log into Microsoft SQL Server
2. Update A4 DB with new tables and records
3. Provide ERD (screenshot and file)
4. SQL Questions
5. Chapter Questions

#### README.md file should include the following items:

* Screenshot of ERD
* Link to a5.sql

#### Assignment Screenshots:

*Screenshot ERD*:

![ERD Screenshot](img/ERD.png)

#### Links:

*Link to a5.sql*
[a5.sql File](lis3781_a5_pduke.sql "A5 SQL Script")
