SET ANSI_WARNINGS ON;
GO

-- connect to DB
use master;
GO

-- drop existing DB
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'pad19b')
DROP DATABASE pad19b;
GO

-- create DB
IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'pad19b')
CREATE DATABASE pad19b;
GO

use pad19b;
GO

-- ------------------------------------------------
-- *** T-SQL ***
-- ------------------------------------------------

-- ------------------------------------------------
-- Table - person
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.person', N'U') IS NOT NULL
DROP TABLE dbo.person;
GO

CREATE TABLE dbo.person
(
    per_id SMALLINT NOT NULL identity(1,1),
    per_ssn BINARY(64) NULL,
    per_salt BINARY(64) NULL,
    per_fname VARCHAR(15) NOT NULL,
    per_lname VARCHAR(30) NOT NULL,
    per_gender CHAR(1) NOT NULL CHECK (per_gender IN ('m','f')),
    per_dob DATE NOT NULL,
    per_street VARCHAR(30) NOT NULL,
    per_city VARCHAR(30) NOT NULL,
    per_state CHAR(2) NOT NULL DEFAULT 'FL',
    per_zip VARCHAR(9) NOT NULL CHECK (per_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    per_email VARCHAR(100) NULL,
    per_type CHAR(1) NOT NULL CHECK (per_type IN ('c','s')),
    per_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    -- ssn and state ID unique constraint
    CONSTRAINT ux_per_ssn UNIQUE nonclustered (per_ssn ASC)
);
GO

-- ------------------------------------------------
-- Table - phone
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.phone', N'U') IS NOT NULL
DROP TABLE dbo.phone;
GO

CREATE TABLE dbo.phone
(
    phn_id SMALLINT NOT NULL identity(1,1),
    per_id SMALLINT NOT NULL,
    phn_num BIGINT NOT NULL CHECK (phn_num LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    phn_type CHAR(1) NOT NULL CHECK (phn_type IN ('h','c','w','f')),
    phn_notes VARCHAR(255) NULL,
    PRIMARY KEY (phn_id),

    CONSTRAINT fk_phone_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - customer
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.customer', N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

CREATE TABLE dbo.customer
(
    per_id SMALLINT NOT NULL,
    cus_balance DECIMAL(7,2) NOT NULL CHECK (cus_balance >= 0),
    cus_total_sales DECIMAL(7,2) NOT NULL CHECK (cus_total_sales >= 0),
    cus_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_customer_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - slsrep
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.slsrep', N'U') IS NOT NULL
DROP TABLE dbo.slsrep;
GO

CREATE TABLE dbo.slsrep
(
    per_id SMALLINT NOT NULL,
    srp_yr_sales_goal DECIMAL(8,2) NOT NULL CHECK (srp_yr_sales_goal >= 0),
    srp_ytd_sales DECIMAL(8,2) NOT NULL CHECK (srp_ytd_sales >= 0),
    srp_ytd_comm DECIMAL(7,2) NOT NULL CHECK (srp_ytd_comm >= 0),
    srp_notes VARCHAR(45) NULL,
    PRIMARY KEY (per_id),

    CONSTRAINT fk_slsrep_person
    FOREIGN KEY (per_id)
    REFERENCES dbo.person (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - srp_hist
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.srp_hist', N'U') IS NOT NULL
DROP TABLE dbo.srp_hist;
GO

CREATE TABLE dbo.srp_hist
(
    sht_id SMALLINT NOT NULL identity(1,1),
    per_id SMALLINT NOT NULL,
    sht_type CHAR(1) NOT NULL CHECK (sht_type IN ('i','u','d')),
    sht_modified DATETIME NOT NULL,
    sht_modifier VARCHAR(45) NOT NULL DEFAULT system_user,
    sht_date DATE NOT NULL DEFAULT getDate(),
    sht_yr_sales_goal DECIMAL(8,2) NOT NULL CHECK (sht_yr_sales_goal >= 0),
    sht_ytd_sales DECIMAL(8,2) NOT NULL CHECK (sht_ytd_sales >= 0),
    sht_ytd_comm DECIMAL(7,2) NOT NULL CHECK (sht_ytd_comm >= 0),
    sht_notes VARCHAR(45) NULL,
    PRIMARY KEY (sht_id),

    CONSTRAINT fk_srp_hist_slsrep
    FOREIGN KEY (per_id)
    REFERENCES dbo.slsrep (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - contact
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.contact', N'U') IS NOT NULL
DROP TABLE dbo.contact;
GO

CREATE TABLE dbo.contact
(
    cnt_id INT NOT NULL identity(1,1),
    per_cid SMALLINT NOT NULL,
    per_sid SMALLINT NOT NULL,
    cnt_date DATETIME NOT NULL,
    cnt_notes VARCHAR(255) NULL,
    PRIMARY KEY (cnt_id),

    CONSTRAINT fk_contact_customer
    FOREIGN KEY (per_cid)
    REFERENCES dbo.customer (per_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_contrat_slsrep
    FOREIGN KEY (per_sid)
    REFERENCES dbo.slsrep (per_id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
GO

-- ------------------------------------------------
-- Table - [order]
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.[order]', N'U') IS NOT NULL
DROP TABLE dbo.[order];
GO

CREATE TABLE dbo.[order]
(
    ord_id INT NOT NULL identity(1,1),
    cnt_id INT NOT NULL,
    ord_placed_date DATETIME NOT NULL,
    ord_filled_date DATETIME NULL,
    ord_notes VARCHAR(255) NULL,
    PRIMARY KEY (ord_id),

    CONSTRAINT fk_order_contact
    FOREIGN KEY (cnt_id)
    REFERENCES dbo.contact (cnt_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - region
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.region', N'U') IS NOT NULL
DROP TABLE dbo.region;
GO

CREATE TABLE dbo.region
(
    reg_id TINYINT NOT NULL identity(1,1),
    reg_name CHAR(1) NOT NULL,
    reg_notes VARCHAR(255) NULL,
    PRIMARY KEY (reg_id)
);
GO

-- ------------------------------------------------
-- Table - state
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.state', N'U') IS NOT NULL
DROP TABLE dbo.state;
GO

CREATE TABLE dbo.state
(
    ste_id TINYINT NOT NULL identity(1,1),
    reg_id TINYINT NOT NULL,
    ste_name CHAR(2) NOT NULL,
    ste_notes VARCHAR(255) NULL,
    PRIMARY KEY (ste_id),
    
    CONSTRAINT fk_state_region
    FOREIGN KEY (reg_id)
    REFERENCES dbo.region (reg_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - city
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.city', N'U') IS NOT NULL
DROP TABLE dbo.city;
GO

CREATE TABLE dbo.city
(
    cty_id SMALLINT NOT NULL identity(1,1),
    ste_id TINYINT NOT NULL,
    cty_name VARCHAR(30) NOT NULL,
    cty_notes VARCHAR(255) NULL,
    PRIMARY KEY (cty_id),
    
    CONSTRAINT fk_city_state
    FOREIGN KEY (ste_id)
    REFERENCES dbo.state (ste_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - store
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.store', N'U') IS NOT NULL
DROP TABLE dbo.store;
GO

CREATE TABLE dbo.store
(
    str_id SMALLINT NOT NULL identity(1,1),
    cty_id SMALLINT NOT NULL,
    str_name VARCHAR(45) NOT NULL,
    str_street VARCHAR(30) NOT NULL,
    str_city VARCHAR(30) NOT NULL,
    str_state CHAR(2) NOT NULL DEFAULT 'FL',
    str_zip INT NOT NULL CHECK (str_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_phone BIGINT NOT NULL CHECK (str_phone LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    str_email VARCHAR(100) NOT NULL,
    str_url VARCHAR(100) NOT NULL,
    str_notes VARCHAR(255) NULL,
    PRIMARY KEY (str_id),

    CONSTRAINT fk_store_city
    FOREIGN KEY (cty_id)
    REFERENCES dbo.city (cty_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - invoice
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.invoice', N'U') IS NOT NULL
DROP TABLE dbo.invoice;
GO

CREATE TABLE dbo.invoice
(
    inv_id INT NOT NULL identity(1,1),
    ord_id INT NOT NULL,
    str_id SMALLINT NOT NULL,
    inv_date DATETIME NOT NULL,
    inv_total DECIMAL(8,2) NOT NULL CHECK (inv_total >= 0),
    inv_paid BIT NOT NULL,
    inv_notes VARCHAR(255) NULL,
    PRIMARY KEY (inv_id),

    CONSTRAINT ux_ord_id UNIQUE nonclustered (ord_id ASC),

    CONSTRAINT fk_invoice_order
    FOREIGN KEY (ord_id)
    REFERENCES dbo.[order] (ord_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_invoice_store
    FOREIGN KEY (str_id)
    REFERENCES dbo.store (str_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - payment
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.payment', N'U') IS NOT NULL
DROP TABLE dbo.payment;
GO

CREATE TABLE dbo.payment
(
    pay_id INT NOT NULL identity(1,1),
    inv_id INT NOT NULL,
    pay_date DATETIME NOT NULL,
    pay_amt DECIMAL(7,2) NOT NULL CHECK (pay_amt >= 0),
    pay_notes VARCHAR(255) NULL,
    PRIMARY KEY (pay_id),

    CONSTRAINT fk_payment_invoice
    FOREIGN KEY (inv_id)
    REFERENCES dbo.invoice (inv_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - vendor
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.vendor', N'U') IS NOT NULL
DROP TABLE dbo.vendor;
GO

CREATE TABLE dbo.vendor
(
    ven_id SMALLINT NOT NULL identity(1,1),
    ven_name VARCHAR(45) NOT NULL,
    ven_street VARCHAR(30) NOT NULL,
    ven_city VARCHAR(30) NOT NULL,
    ven_state CHAR(2) NOT NULL DEFAULT 'FL',
    ven_zip INT NOT NULL CHECK (ven_zip LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_phone BIGINT NOT NULL CHECK (ven_phone LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
    ven_email VARCHAR(100) NOT NULL,
    ven_url VARCHAR(100) NOT NULL,
    ven_notes VARCHAR(255) NULL,
    PRIMARY KEY (ven_id)
);
GO

-- ------------------------------------------------
-- Table - product
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.product', N'U') IS NOT NULL
DROP TABLE dbo.product;
GO

CREATE TABLE dbo.product
(
    pro_id SMALLINT NOT NULL identity(1,1),
    ven_id SMALLINT NOT NULL,
    pro_name VARCHAR(30) NOT NULL,
    pro_descript VARCHAR(45) NULL,
    pro_weight FLOAT NOT NULL CHECK (pro_weight >= 0),
    pro_qoh SMALLINT NOT NULL CHECK (pro_qoh >= 0),
    pro_cost DECIMAL(7,2) NOT NULL CHECK (pro_cost >= 0),
    pro_price DECIMAL(7,2) NOT NULL CHECK (pro_price >= 0),
    pro_discount DECIMAL(3,0) NULL,
    pro_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id),

    CONSTRAINT fk_product_vendor
    FOREIGN KEY (ven_id)
    REFERENCES dbo.vendor (ven_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - product_hist
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.product_hist', N'U') IS NOT NULL
DROP TABLE dbo.product_hist;
GO

CREATE TABLE dbo.product_hist
(
    pht_id INT NOT NULL identity(1,1),
    pro_id SMALLINT NOT NULL,
    pht_date DATETIME NOT NULL,
    pht_cost DECIMAL(7,2) NOT NULL CHECK (pht_cost >= 0),
    pht_price DECIMAL(7,2) NOT NULL CHECK (pht_price >= 0),
    pht_discount DECIMAL(3,0) NULL,
    pht_notes VARCHAR(255) NULL,
    PRIMARY KEY (pht_id),

    CONSTRAINT fk_product_hist_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - order_line
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.order_line', N'U') IS NOT NULL
DROP TABLE dbo.order_line;
GO

CREATE TABLE dbo.order_line
(
    oln_id INT NOT NULL identity(1,1),
    ord_id INT NOT NULL,
    pro_id SMALLINT NOT NULL,
    oln_qty SMALLINT NOT NULL CHECK (oln_qty >= 0),
    oln_price DECIMAL(7,2) NOT NULL CHECK (oln_price >= 0),
    oln_notes VARCHAR(255) NULL,
    PRIMARY KEY (oln_id),

    CONSTRAINT fk_order_line_order
    FOREIGN KEY (ord_id)
    REFERENCES dbo.[order] (ord_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT fk_order_line_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

-- ------------------------------------------------
-- Table - time
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time;
GO

CREATE TABLE dbo.time
(
    tim_id INT NOT NULL identity(1,1),
    tim_yr SMALLINT NOT NULL,
    tim_qtr TINYINT NOT NULL,
    tim_month TINYINT NOT NULL,
    tim_week TINYINT NOT NULL,
    tim_day TINYINT NOT NULL,
    tim_time TIME NOT NULL,
    tim_notes VARCHAR(255) NULL,
    PRIMARY KEY (tim_id)
);
GO

-- ------------------------------------------------
-- Table - sale
-- ------------------------------------------------
IF OBJECT_ID (N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
GO

CREATE TABLE dbo.sale
(
    pro_id SMALLINT NOT NULL,
    str_id SMALLINT NOT NULL,
    cnt_id INT NOT NULL,
    tim_id INT NOT NULL,
    sal_qty SMALLINT NOT NULL,
    sal_price DECIMAL(8,2) NOT NULL,
    sal_total DECIMAL(8,2) NOT NULL,
    sal_notes VARCHAR(255) NULL,
    PRIMARY KEY (pro_id, str_id, cnt_id, tim_id),

    CONSTRAINT ux_pro_id_str_id_cnt_id_tim_id
    unique nonclustered (pro_id ASC, str_id ASC, cnt_id ASC, tim_id ASC),

    CONSTRAINT fk_sale_time
    FOREIGN KEY (tim_id)
    REFERENCES dbo.time (tim_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT k_sale_contact
    FOREIGN KEY (cnt_id)
    REFERENCES dbo.contact (cnt_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT k_sale_store
    FOREIGN KEY (str_id)
    REFERENCES dbo.store (str_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    CONSTRAINT k_sale_product
    FOREIGN KEY (pro_id)
    REFERENCES dbo.product (pro_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);
GO

SELECT * FROM information_schema.tables;

-- ------------------------------------------------
-- *** Data Inserts ***
-- ------------------------------------------------

-- ------------------------------------------------
-- Data - person
-- ------------------------------------------------
INSERT INTO dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1, NULL, 'Steve', 'Rogers', 'm', '1923-10-03', '437 Southern Drive', 'Rochester', 'NY', '324402222', 'srogers@comcast.net', 's', NULL),
(2, NULL, 'Bruce', 'Wayne', 'm', '1968-03-20', '1007 Mountain Drive', 'Gotham', 'NY', '983208440', 'bwayne@knology.net', 's', NULL),
(3, NULL, 'Peter', 'Parker', 'm', '1988-09-12', '20 Ingram St', 'New York', 'NY', '102862341', 'pparker@msn.com', 's', NULL),
(4, NULL, 'Jane', 'Thompson', 'f', '1978-05-08', '135 Ocean View Dr', 'Seattle', 'WA', '032084409', 'jthompson@gmail.com', 's', NULL),
(5, NULL, 'Debra', 'Steele', 'f','1994-07-19', '543 Oak Ln', 'Milwaukee', 'WI', '286234178', 'dsteele@verizon.net', 's', NULL),
(6, NULL, 'Tony', 'Stark', 'm', '1972-05-04', '332 Palm Ave', 'Malibu', 'CA', '902638332', 'tstart@yahoo.com', 'c', NULL),
(7, NULL,  'Hank', 'Pymil', 'm', '1980-08-28', '2355 Brown St', 'Cleveland', 'OH', '022348890', 'hpym@aol.com', 'c', NULL),
(8, NULL, 'Bob', 'Best', 'm', '1992-02-10', '492 Avendale Ave', 'Scottsdale', 'AZ', '872638332', 'bbest@yahoo.com', 'c', NULL),
(9, NULL,  'Sandra', 'Dole', 'f', '1990-01-26', '8791 Lawrence Ave', 'Atlanta', 'GA', '002348890', 'sdole@gmail.com', 'c', NULL),
(10, NULL, 'Ben', 'Avery', 'm', '1983-12-24', '6432 Thunder Ln', 'Sioux Falls', 'SD', '562638332', 'bavery@hotmail.com', 'c', NULL),
(11, NULL, 'Arthur', 'Curry', 'm', '1975-12-15', '3304 Euclid Ave', 'Miami', 'FL', '000219932', 'acurry@gmail.com', 'c', NULL),
(12, NULL, 'Diana', 'Price', 'f', '1980-08-22', '944 Green St', 'Las Vegas', 'NV', '332048823', 'dprice@symaptico.com', 'c', NULL),
(13, NULL,  'Adam', 'Jurris', 'm', '1995-01-31', '98435 Valencia Dr', 'Gulf Shores', 'AL', '870219932', 'ajurris@gmx.com', 'c', NULL),
(14, NULL, 'Judy', 'Sleen', 'f','1970-03-22', '56343 Rover Ct', 'Billings', 'MT', '672048823', 'jsleen@symaptico.com', 'c', NULL),
(15, NULL,  'Bill', 'Neiderheim', 'm', '1982-03-13', '4375 Netherland Rd', 'South Bend', 'IN', '320219932', 'bneiderheim@comcast.net', 'c', NULL);

select * from dbo.person;
GO
-- -----------------------------------------------
-- populate person table with hashed and salted SSN numbers
CREATE PROC dbo.CreatePersonSSN
AS
BEGIN

DECLARE @salt BINARY(64);
DECLARE @ran_num INT;
DECLARE @ssn BINARY(64);
DECLARE @x INT, @y INT;
SET @x = 1;
SET @y = (select count(*) from dbo.person);

    WHILE (@x <= @y)
    BEGIN

    SET @salt = CRYPT_GEN_RANDOM(64);
    SET @ran_num = FLOOR(RAND()*(999999999-111111111+1))+111111111;
    SET @ssn = HASHBYTES('SHA2_512', concat(@salt, @ran_num));

    update dbo.person
    set per_ssn = @ssn, per_salt = @salt
    where per_id = @x;

    SET @x = @x + 1;

    END;

END;
GO

exec dbo.CreatePersonSSN

-- ------------------------------------------------
-- Data - phone
-- ------------------------------------------------
INSERT INTO dbo.phone
(per_id, phn_num, phn_type, phn_notes)
VALUES
(1, '8505469982', 'h', NULL),
(2, '2456897784', 'c', NULL),
(3, '3546518879', 'w', NULL),
(4, '5546879984', 'h', NULL),
(5, '6598813351', 'c', NULL),
(6, '5567784952', 'w', NULL),
(7, '1578695568', 'h', NULL),
(8, '6689953457', 'c', NULL),
(9, '5469813276', 'w', NULL),
(10, '9546751328', 'h', NULL),
(11, '5678849135', 'c', NULL),
(12, '2567859978', 'w', NULL),
(13, '5567884895', 'h', NULL),
(14, '6897785567', 'c', NULL),
(15, '8894568879', 'f', 'fax lol');

select * from dbo.phone;
GO
-- ------------------------------------------------
-- Data - slsrep
-- ------------------------------------------------
INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL),
(2, 80000, 35000, 3500, NULL),
(3, 150000, 84000, 9650, 'great salesperson!'),
(4, 125000, 870000, 15300, NULL),
(5, 98000, 43000, 8750, NULL);

select * from dbo.slsrep;
GO
-- ------------------------------------------------
-- Data - customer
-- ------------------------------------------------
INSERT INTO dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 234.92, NULL),
(8, 0, 4578, NULL),
(9, 981.73, 1672.38, NULL),
(10, 541.23, 782.57, NULL),
(11, 251.02, 13782.69, NULL),
(12, 582.67, 963.12, NULL),
(13, 121.67, 1057.45, NULL),
(14, 765.43, 6789.42, NULL),
(15, 304.36, 456.91, NULL);

select * from dbo.customer;
GO
-- ------------------------------------------------
-- Data - contact
-- ------------------------------------------------
INSERT INTO dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
VALUES
( 1, 6, '1999-01-01', NULL),
( 2, 6, '2001-09-29', NULL),
( 3, 7, '2002-08-15', NULL),
( 2, 7, '2002-09-01', NULL),
( 3, 8, '2004-03-03', NULL),
( 4, 11, '2004-04-07', NULL),
( 5, 13, '2005-05-02', NULL),
( 4, 15, '2005-07-02', NULL);

select * from dbo.contact;
GO
-- ------------------------------------------------
-- Data - [order]
-- ------------------------------------------------
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-24', NULL),
(2, '2005-03-19', '2005-07-28', NULL),
(3, '2011-07-01', '2011-07-06', NULL),
(4, '2009-12-24', '2010-01-05', NULL),
(5, '2008-09-21', '2008-11-26', NULL),
(6, '2010-05-13', '2010-06-07', NULL),
(7, '2007-09-02', '2007-10-15', NULL),
(8, '2012-02-21', '2012-05-02', NULL);

select * from dbo.[order];
GO

-- ------------------------------------------------
-- Data - region
-- ------------------------------------------------
INSERT INTO dbo.region
(reg_name, reg_notes)
VALUES
('c', NULL),
('n', NULL),
('e', NULL),
('s', NULL),
('w', NULL);

select * from dbo.region;
GO

-- ------------------------------------------------
-- Data - state
-- ------------------------------------------------
INSERT INTO dbo.state
(reg_id, ste_name, ste_notes)
VALUES
(1, 'MI', NULL),
(3, 'IL', NULL),
(4, 'WA', NULL),
(5, 'FL', NULL),
(2, 'TX', NULL);

select * from dbo.state;
GO

-- ------------------------------------------------
-- Data - city
-- ------------------------------------------------
INSERT INTO dbo.city
(ste_id, cty_name, cty_notes)
VALUES
(1, 'Detroit', NULL),
(2, 'Aspen', NULL),
(2, 'Chicago', NULL),
(3, 'Clover', NULL),
(4, 'St. Louis', NULL);

select * from dbo.city;
GO

-- ------------------------------------------------
-- Data - store
-- ------------------------------------------------
INSERT INTO dbo.store
(cty_id, str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
(2, 'Walgreens', '14567 Walnut Ln', 'Aspen', 'CO', '475315690', '3127658127', 'info@walgreens.com', 'http://www.walgreens.com', NULL),
(3, 'CVS', '572 Casper Rd', 'Chicago', 'IL', '505231519', '3126926534', 'help@cvs.com', 'http://www.cvs.com', NULL),
(4, 'Lowes', '81309 Catipult Ave', 'Colver', 'WA', '802345617', '9017563421', 'sales@lowes.com', 'http://www.lowes.com', NULL),
(5, 'Walmart', '14567 Walnut Ln', 'St. Louis', 'FL', '387563628', '8722718923', 'info@walmart.com', 'http://www.walmart.com', NULL),
(1, 'Dollar General', '47583 Davidson Rd', 'Detroit', 'MI', '482983456', '3137583492', 'ask@dollargeneral.com', 'http://www.dollargeneral.com', 'recently sold property');

select * from dbo.store;
GO
-- ------------------------------------------------
-- Data - invoice
-- ------------------------------------------------
INSERT INTO dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
VALUES
(1, 1, '2010-11-23', 58.32, 0, NULL),
(2, 1, '2005-03-21', 100.59, 0, NULL),
(3, 1, '2011-07-02', 57.34, 0, NULL),
(4, 2, '2009-12-26', 99.32, 1, NULL),
(5, 2, '2008-09-22', 1109.67, 1, NULL),
(6, 3, '2010-05-14', 239.83, 1, NULL),
(7, 4, '2007-09-03', 537.29, 0, NULL),
(8, 5, '2012-02-22', 644.21, 1, NULL);

select * from dbo.invoice;
GO
-- ------------------------------------------------
-- Data - vendor
-- ------------------------------------------------
INSERT INTO dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
VALUES
('Sysco', '531 Dolphin Rd', 'Orlando', 'FL', '344761234', '7641238543', 'sales@sysco.com', 'http://www.sysco.com', NULL),
('General Electic', '100 Happy Trails Dr.', 'Boston', 'MA', '123458743', '2134569641', 'support@ge.com', 'http://www.ge.com', 'very good turnaround'),
('Cisco', '300 Cisco Dr.', 'Stanford', 'OR', '872315492', '7823456723', 'cisco@cisco.com', 'http://www.cisco.com', NULL),
('Goodyear', '100 Good Dr', 'Gary', 'IN', '485321956', '5784218427', 'sales@goodyear.com', 'http://www.goodyear.com', NULL),
('Craftsman', '42185 Magenta Ave', 'Lake Falls', 'ND', '387513649', '9197345632', 'support@craftsman.com', 'http://www.craftsman.com', 'great tools');

select * from dbo.vendor;
GO
-- ------------------------------------------------
-- Data - product
-- ------------------------------------------------
INSERT INTO dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
VALUES
(1, 'hammer', 'hammer things', 2.5, 45, 4.99, 7.99, 30, 'buy one get one free'),
(2, 'screwdriver', 'screw things', 1.8, 120, 1.99, 3.49, NULL, NULL),
(4, 'pail', 'pail in comparison', 2.8, 48, 3.89, 7.99, 40, NULL),
(5, 'cooking oil', 'cook some fish', 15, 19, 19.99, 28.99, NULL, 'gallons'),
(3, 'frying pan', 'fry some fish', 3.5, 178, 8.45, 13.99, 50, 'top included');

select * from dbo.product;
GO
-- ------------------------------------------------
-- Data - order_line
-- ------------------------------------------------
INSERT INTO dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
VALUES
(1, 2, 10, 8.0, NULL),
(2, 3, 7, 9.88, NULL),
(4, 4, 3, 6.99, NULL),
(5, 1, 2, 12.76, NULL),
(7, 5, 13, 58.99, NULL);

select * from dbo.order_line;
GO
-- ------------------------------------------------
-- Data - payment
-- ------------------------------------------------
INSERT INTO dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
VALUES
(1, '2010-12-05', 5.99, NULL),
(2, '2005-04-16', 4.99, NULL),
(3, '2011-07-16', 8.75, NULL),
(4, '2010-01-09', 19.55, NULL),
(5, '2008-10-02', 32.50, NULL),
(6, '2010-05-23', 20.00, NULL),
(7, '2007-09-12', 100.99, NULL),
(8, '2012-03-04', 103.68, NULL);

select * from dbo.payment;
GO
-- ------------------------------------------------
-- Data - product_hist
-- ------------------------------------------------
INSERT INTO dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
VALUES
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'buy one get one free'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, NULL, NULL),
(4, '2005-03-04 23:21:49', 3.89, 7.99, 40, NULL),
(5, '2005-05-06 18:09:04', 19.99, 28.99, NULL, 'gallons'),
(3, '2006-05-07 15:07:29', 8.45, 13.99, 50, 'top included');

select * from dbo.product_hist;
GO

-- ------------------------------------------------
-- Data - time
-- ------------------------------------------------
INSERT INTO dbo.time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
VALUES
(2008, 2, 5, 19, 7, '11:59:59', NULL),
(20010, 4, 12, 49, 4, '08:34:21', NULL),
(1999, 4, 12, 52, 5, '05:21:34', NULL),
(2011, 3, 8, 36, 1, '09:32:18', NULL),
(2001, 3, 7, 27, 2, '23:56:32', NULL);

select * from dbo.time;
GO

-- ------------------------------------------------
-- Data - sale
-- ------------------------------------------------
INSERT INTO dbo.sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
VALUES
(1, 1, 8, 1, 20, 11.59, 199.8, NULL),
(2, 1, 8, 1, 5, 11.59, 139.8, NULL),
(2, 1, 7, 1, 6, 10.59, 159.8, NULL),
(3, 1, 7, 2, 8, 17.59, 189.8, NULL),
(3, 2, 6, 2, 30, 14.59, 119.8, NULL),
(3, 2, 8, 2, 5, 3.59, 215.99, NULL),
(3, 5, 6, 5, 12, 9.59, 213.99, NULL),
(4, 5, 5, 5, 17, 7.59, 217.99, NULL),
(4, 5, 4, 4, 21, 8.59, 212.99, NULL),
(5, 4, 6, 3, 6, 5.59, 214.99, NULL),
(5, 4, 4, 3, 9, 55.69, 236.89, NULL),
(5, 4, 3, 3, 10, 54.69, 239.89, NULL),
(1, 3, 3, 4, 11, 53.69, 233.89, NULL),
(1, 3, 3, 5, 18, 52.69, 235.89, NULL),
(1, 3, 2, 4, 12, 51.69, 230.89, NULL),
(2, 2, 2, 2, 9, 75.39, 253.18, NULL),
(3, 3, 2, 1, 25, 73.39, 257.18, NULL),
(4, 3, 3, 3, 6, 77.39, 258.18, NULL),
(4, 2, 3, 4, 17, 78.39, 252.18, NULL),
(4, 4, 3, 2, 15, 74.39, 251.18, NULL),
(5, 4, 1, 3, 10, 20.8, 84.55, NULL),
(5, 5, 1, 5, 6, 24.8, 89.55, NULL),
(2, 1, 2, 4, 28, 27.8, 83.55, NULL),
(2, 2, 1, 2, 9, 29.8, 80.55, NULL),
(2, 2, 1, 3, 1, 23.8, 85.55, NULL);


select * from dbo.sale;
GO

-- ------------------------------------------------
-- Data - srp_hist
-- ------------------------------------------------
INSERT INTO dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_ytd_sales, sht_ytd_comm, sht_notes)
VALUES
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL),
(4, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL),
(2, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL);

select * from dbo.srp_hist;
GO

