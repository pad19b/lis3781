> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Patrick Duke

### Assignment A1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Instillation
3. Questions
4. Entity Relationship Diagram and SQL Code (optional)
5. Bitbucket repo links:
   a) this assignment 
   b) the bitbucket totrial (bitbucketstationlocations)

#### README.md file should include the following items:

* Screenshot of ERD 
* Ex1 SQL Solution
* git commands with short descriptions


> #### Git commands w/short descriptions:
>
>1. git init - create an empty Git repository or reinitialize an existing one
>2. git status - show the working tree status
>3. git add - add file contents to the index
>4. git commit - record changes to the repository
>5. git push - update remote refs along with associated objects
>6. git pull - fetch from and integrate with another repository or a local branch
>7. git log - show commit logs

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost/ampps:

![AMPPS Installation Screenshot](AMPPS_Install.png)

*Screenshot of A1 ERD:

![Screenshot of A1 ERD](a1_erd.png)

*Screenshot of A1 Ex1:

![Screenshot of A1 EX1](a1_ex1.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[Bitbucket Station Locations Link](https://bitbucket.org/pad19b/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
